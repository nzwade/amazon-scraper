# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class Product(scrapy.Item):
    title = scrapy.Field()
    brand = scrapy.Field()
    manufacturer = scrapy.Field()
    country_of_origin = scrapy.Field()
    pet_type = scrapy.Field()
    rating_average = scrapy.Field()
    rating_count = scrapy.Field()
    price = scrapy.Field()
    price_currency_symbol = scrapy.Field()
    images = scrapy.Field()
    image_urls = scrapy.Field()
    url = scrapy.Field()
    asin = scrapy.Field()
