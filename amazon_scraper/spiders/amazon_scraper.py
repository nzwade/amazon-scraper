import json
import os
import re

import scrapy
from scrapy.exceptions import CloseSpider

from ..items import Product


class AmazonScraper(scrapy.Spider):
    asin_url_regex = r'dp(?:/|%2F)(\S{10})(?:/|%2F)'

    name = "amazon_scraper"

    # How many pages you want to scrape
    no_of_pages = 7

    asins_already_scraped = []

    # Headers to fix 503 service unavailable error
    # Spoof headers to force servers to think that request coming from browser ;)
    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.2840.71 Safari/539.36'}

    def start_requests(self):
        self.set_asins_already_scraped()
        # starting urls for scraping
        urls = ["https://www.amazon.co.uk/s?k=mens+face+moisturiser&ref=nb_sb_noss"]

        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse, headers=self.headers)

    def set_asins_already_scraped(self):
        # import ipdb;ipdb.set_trace()
        if os.path.exists('./data/data.json') and os.path.getsize('./data/data.json'):
            with open('./data/data.json', 'r') as f:
                data = json.load(f)
                if data:
                    self.asins_already_scraped = [product['asin'] for product in data]

    def parse(self, response):
        if 'Enter the characters you see below' in response.xpath("//body//text()").getall():
            raise CloseSpider('bandwidth_exceeded')

        self.no_of_pages -= 1

        products = response.xpath("//a[@class='a-link-normal a-text-normal']").xpath("@href").getall()

        for product in products:
            self.logger.info(product)
            asin = re.search(self.asin_url_regex, product)
            # For some reason there are two possible url formats:
            # /Bulldog-LWT1004-Original-Moisturiser-100ml/dp/B00822YOIM/ref=sr_1_5?dchild=1&keywords=mens+face+moisturiser&qid=1603486798&sr=8-5
            # /gp/slredirect/picassoRedirect.html/ref=pa_sp_atf_aps_sr_pg1_1?ie=UTF8&adId=A02636032D3N3WGKN2YCH&url=%2FLOreal-Expert-Matte-Anti-Shine-Moisturiser%2Fdp%2FB000T1XQLA%2Fref%3Dsr_1_1_sspa%3Fdchild%3D1%26keywords%3Dmens%2Bface%2Bmoisturiser%26qid%3D1603486991%26sr%3D8-1-spons%26psc%3D1&qualifier=1603486991&id=2559457788839229&widgetName=sp_atf
            # TODO still seeing some duplicate ASIN in results? investigate
            if asin:
                asin = asin.group(1)
                if asin in self.asins_already_scraped:
                    self.logger.info(f'{asin} already scraped. Skipping.')
                    continue
                self.asins_already_scraped.append(asin)
            else:
                self.logger.error(f'Unable to identify ASIN in url {product}')
                continue

            final_url = response.urljoin(product)
            yield scrapy.Request(url=final_url, callback=self.parse_product, headers=self.headers)

        # print(response.body)
        # title = response.xpath("//span[@class='a-size-medium a-color-base a-text-normal']//text()").getall()
        # title = response.css('span').getall()
        # print(title)

        if self.no_of_pages > 0:
            next_page_url = response.xpath("//ul[@class='a-pagination']/li[@class='a-last']/a").xpath("@href").get()
            final_url = response.urljoin(next_page_url)
            yield scrapy.Request(url=final_url, callback=self.parse, headers=self.headers)

    def parse_product(self, response):
        if 'Enter the characters you see below' in response.xpath("//body//text()").getall():
            raise CloseSpider('bandwidth_exceeded')

        url = response.url

        asin = re.search(self.asin_url_regex, url)
        if asin:
            asin = asin.group(1)

        title = (response.xpath("//span[@id='productTitle']//text()").get()
                 or response.xpath("//h1[@id='title']//text()").get()).strip()

        brand = self.get_field_from_technical_details(response, '\nBrand\n')
        # Note some products have no brand
        # https://www.amazon.co.uk/Elemis-Daily-Moisture-Hydrating-Lotion/dp/B00175U4L2
        # https://www.amazon.co.uk/Elemis-Gentle-Rose-Exfoliator-Smoothing/dp/B00CLDSVDM
        country_of_origin = self.get_field_from_technical_details(response, '\nCountry of Origin\n')
        pet_type = self.get_field_from_technical_details(response, '\nPet Type\n')  # To catch products for females
        # TODO add support for this format: https://www.amazon.co.uk/Neutrogena-Spectrum-Sunscreen-Moisturizer-Irritation/dp/B003NSPOF2/ref=sr_1_93?dchild=1&keywords=mens+face+moisturiser&qid=1603117021&sr=8-93
        # (currently no brand is booking?)
        manufacturer = self.get_field_from_technical_details(response, '\nManufacturer\n')
        if not manufacturer:
            manufacturer = self.get_field_from_product_details(response, 'Manufacturer\n:\n')

        raw_rating = response.xpath("//div[@id='averageCustomerReviews_feature_div']").xpath("//span[@class='a-icon-alt']//text()").get()
        # https://www.amazon.co.uk/Essential-Boutique-Sandalwood-Patchouli-Moisturiser/dp/B08GPCVPD3
        if raw_rating == 'Previous page of related Sponsored Products':
            rating_average = None
        else:
            rating_average = float(raw_rating.strip().replace(' out of 5 stars', '')) if raw_rating else None

        raw_rating_count = response.xpath("//span[@id='acrCustomerReviewText']//text()").get()
        rating_count = int(raw_rating_count.strip().replace(',', '').replace(' ratings', '').replace(' rating', '')) if raw_rating_count else None

        price = (response.xpath("//span[@id='priceblock_ourprice']//text()")
                 or response.xpath("//span[@id='priceblock_dealprice']//text()"))
        if len(price) > 1:
            price = price[1].get()
        elif len(price) == 1:
            price = price[0].get()
        else:
            price = price.get()
        price_currency_symbol = None
        if price:
            price = price.strip()
            price_currency_symbol = price[:1]
            price = float(price[1:])

        # use data-a-dynamic-image for this case:
        # https://www.amazon.co.uk/LOreal-Expert-Pure-Charcoal-Oily/dp/B07V2J83QS
        landing_image_urls = json.loads(response.xpath("//img[@id='landingImage']/@data-a-dynamic-image").get())
        landing_image_urls = [key for key, value in landing_image_urls.items()]
        landing_image = landing_image_urls[0]  # Might not be the highest resolution but its the easiest to get for now.

        # Note there is this unusual image format:
        # https://www.amazon.co.uk/No7-Protect-Perfect-Intense-Advanced/dp/B00ZXKDVMQ

        img_url = (response.xpath("//img[@id='landingImage']/@data-old-hires").get()
                   or response.xpath("//img[@id='imgBlkFront']/@src").get()
                   or landing_image)

        yield Product(title=title, brand=brand, manufacturer=manufacturer, country_of_origin=country_of_origin,
                      pet_type=pet_type, rating_average=rating_average, rating_count=rating_count, price=price,
                      price_currency_symbol=price_currency_symbol, image_urls=[img_url], url=url, asin=asin)

    @staticmethod
    def get_field_from_product_details(response, field):
        # For pages with product details table:
        # https://www.amazon.co.uk/Elemis-Daily-Moisture-Hydrating-Lotion/dp/B00175U4L2
        product_details_table = response.xpath("//ul[contains(@class,'detail-bullet-list')]/li")
        for row in product_details_table:
            if row.xpath("span[@class='a-list-item']/span[1]//text()").get() == field:
                field_value = row.xpath("span[@class='a-list-item']/span[2]//text()").get()
                return field_value.strip()

    @staticmethod
    def get_field_from_technical_details(response, field):
        field_row_index = None
        table_headers = response.xpath("//table[@id='productDetails_techSpec_section_1']/tr/th//text()").getall()
        for row_index, row in enumerate(table_headers):
            if row == field:
                field_row_index = row_index + 1
                break
        if field_row_index:
            field_value = response.xpath(f"//table[@id='productDetails_techSpec_section_1']/tr[{field_row_index}]/td//text()").get()
            return field_value.strip()
